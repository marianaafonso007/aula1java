package com.company;

import java.util.Random;
import java.util.Scanner;

public class CacaNiquel {
    public static void main(String[] args) {

        //alterar programa criando um lista com valores que devem ser sorteados
        //(Math.Random() * valores.length - 1) + 1

        Scanner leitura = new Scanner(System.in);
        System.out.println("Bora jogar? Digite 1 para SIM e 2 para NÃO!");
        int resposta = leitura.nextInt() ;
        boolean iniciarjogo = resposta != 1 ? false : true;

        if(iniciarjogo){
            do {
                runJogo();
                System.out.println("Deseja continuar jogando? Digite 1 para SIM e N para NÃO!");
                resposta = leitura.nextInt() ;
                iniciarjogo = resposta != 1 ? false : true;
            }while (iniciarjogo);
        }


    }

    public static void runJogo(){
        int[] cacaNiquel = new int[3];
        int vlPrimeiraColuna = 0, vlSegundaColuna = 0, vlTerceiraColuna = 0;
        Random rd = new Random();

        for (int i= 0; i<3; i++){
            cacaNiquel[i] = rd.nextInt(9);
        }

//        Caso hajam três valores diferentes: 0 pontos
//        Caso hajam dois valores iguais: 100 pontos
//        Caso hajam três valores iguais: 1000 pontos
//        Caso hajam três valores "7": 5000 pontos

        for (int i= 0; i < cacaNiquel.length; i++){
            vlPrimeiraColuna = i == 0 ? cacaNiquel[i] : vlPrimeiraColuna;
            vlSegundaColuna = i == 1 ? cacaNiquel[i] : vlSegundaColuna;
            vlTerceiraColuna = i == 2 ? cacaNiquel[i] : vlTerceiraColuna;
        }

        if(vlPrimeiraColuna == vlSegundaColuna && vlTerceiraColuna == vlSegundaColuna){
            if (vlPrimeiraColuna != 7){
                System.out.println("Saída da máquina:" + vlPrimeiraColuna + " " + vlSegundaColuna + " " + vlTerceiraColuna + " Parabéns! Você ganhou: 1000 pontos!");
            } else{
                System.out.println("Saída da máquina:" + vlPrimeiraColuna + " " + vlSegundaColuna + " " + vlTerceiraColuna + " Parabéns! Você ganhou: 5000 pontos!");
            }
        } else if (vlPrimeiraColuna == vlSegundaColuna || vlTerceiraColuna == vlSegundaColuna || vlTerceiraColuna == vlPrimeiraColuna)
        {
            System.out.println("Saída da máquina:" + vlPrimeiraColuna + " " + vlSegundaColuna + " " + vlTerceiraColuna + " Parabéns! Você ganhou: 100 pontos!");
        }else {
            System.out.println("Saída da máquina:" + vlPrimeiraColuna + " " + vlSegundaColuna + " " + vlTerceiraColuna + " Perdeu! Você ganhou: 0 pontos!");
        }
    }
}
