package com.company;

import java.util.Scanner;

public class Piscina {

    public static void main(String[] args) {
        //largura, comprimento, profundidade

        double largura, comprimento, profunidade;
        String inicioMensagem, fimMensagem;
        inicioMensagem= "Digite ";
        fimMensagem = " da Piscina";

        Scanner leitura = new Scanner(System.in);
        retornarTela(inicioMensagem + "a largura" + fimMensagem);
        largura = leitura.nextDouble();
        retornarTela(inicioMensagem + "o comprimento" + fimMensagem);
        comprimento = leitura.nextDouble();
        retornarTela(inicioMensagem + "a profunidade" + fimMensagem);
        profunidade = leitura.nextDouble();

        //8 m x 5 m x 1,5 m = 60 m³ (sessenta metros cúbicos)
        //A medida de 1 m³ (metro cúbico) corresponde a 1000 litros. Portanto, 60 m³ é igual à capacidade de 60 000 litros

        double calculo = largura * comprimento * profunidade;

        retornarTela("Capacidade da piscina é de " + (calculo*1000) + " Litros!");

    }

    public static void retornarTela(String texto){
        System.out.println(texto);
    }
}
