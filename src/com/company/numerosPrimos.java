package com.company;

public class numerosPrimos {
    public static void main(String[] args) {
        //    Natural;
        //    Maior que um; e,
        //    Tem apenas 2 divisores com resto zero, ele próprio e um.

        for(int i = 2; i <= 100; i++){
            boolean ePrimo = true;
            for (int j = 2; j < i; j++){
                if (j%i == 0){
                    ePrimo = false;
                }
            }
            if (ePrimo){
                System.out.println(i);
            }
        }
    }
}
